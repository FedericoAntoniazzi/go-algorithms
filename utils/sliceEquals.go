package utils

// Compare two arrays of int
// Return true if they contain the same elements.
// Otherwise return false
func Equals(a, b []int) bool {

	// One array can't be nil
	if (a == nil) != (b == nil) {
		return false
	}

	// They must have the same length
	if len(a) != len(b) {
		return false
	}

	// Check element one by one
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
