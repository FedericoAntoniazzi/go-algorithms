package utils

func SwapPointers(a, b *int) {
	*a, *b = *b, *a
}
