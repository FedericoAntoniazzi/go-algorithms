package sort

import (
	"testing"

	"gitlab.com/FedericoAntoniazzi/go-algorithms/utils"
)

func TestMergeSort_empty(t *testing.T) {
	arr := []int{}
	expected := []int{}

	Merge(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestMergeSort_sorted(t *testing.T) {
	arr := []int{1, 2, 3, 4}
	expected := []int{1, 2, 3, 4}

	Merge(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestMergeSort_unsorted(t *testing.T) {
	arr := []int{4, 3, 2, 1}
	expected := []int{1, 2, 3, 4}

	Merge(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func BenchmarkMergeSort(b *testing.B) {
	Merge([]int{4, 3, 2, 1})
}
