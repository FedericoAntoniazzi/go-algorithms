package sort

func Insertion(input []int) {
	for i:=1; i<len(input); i++ {
		temp := input[i]

		for ; i>0 && input[i-1] > temp; i-- {
			input[i] = input[i-1]
		}

		input[i] = temp
	}
}
