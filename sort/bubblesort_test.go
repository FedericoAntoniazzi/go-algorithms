package sort

import (
	"testing"

	"gitlab.com/FedericoAntoniazzi/go-algorithms/utils"
)

func TestBubbleSort_empty(t *testing.T) {
	arr := []int{}
	expected := []int{}

	Bubble(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestBubbleSort_sorted(t *testing.T) {
	arr := []int{1, 2, 3, 4}
	expected := []int{1, 2, 3, 4}

	Bubble(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestBubbleSort_unsorted(t *testing.T) {
	arr := []int{4, 3, 2, 1}
	expected := []int{1, 2, 3, 4}

	Bubble(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func BenchmarkBubbleSort(b *testing.B) {
	Bubble([]int{4, 3, 2, 1})
}
