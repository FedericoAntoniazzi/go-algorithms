package sort

import "gitlab.com/FedericoAntoniazzi/go-algorithms/utils"

// Return the index of the minimum element
func findMinimumIndex(arr []int) int {
	min := 0
	for i:=0; i<len(arr); i++ {
		if arr[i] < arr[min] {
			min = i
		}
	}

	return min
}

func Selection(input []int) {

	for i:=0; i<len(input); i++ {

		min := findMinimumIndex(input[i:])
		// Since findMinimumIndex returns the index of the minimum element
		// in the slice, we need to add i in order to get the right index
		min += i

		utils.SwapPointers(&input[min], &input[i])
	}
}
