package sort

import (
	"testing"

	"gitlab.com/FedericoAntoniazzi/go-algorithms/utils"
)

func TestSelectionSort_empty(t *testing.T) {
	arr := []int{}
	expected := []int{}

	Selection(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestSelectionSort_sorted(t *testing.T) {
	arr := []int{1, 2, 3, 4}
	expected := []int{1, 2, 3, 4}

	Selection(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestSelectionSort_unsorted(t *testing.T) {
	arr := []int{4, 3, 2, 1}
	expected := []int{1, 2, 3, 4}

	Selection(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func BenchmarkSelectionSort(b *testing.B) {
	Selection([]int{4, 3, 2, 1})
}
