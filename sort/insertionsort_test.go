package sort

import (
	"testing"

	"gitlab.com/FedericoAntoniazzi/go-algorithms/utils"
)

func TestInsertionSort_empty(t *testing.T) {
	arr := []int{}
	expected := []int{}

	Insertion(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestInsertionSort_sorted(t *testing.T) {
	arr := []int{1, 2, 3, 4}
	expected := []int{1, 2, 3, 4}

	Insertion(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func TestInsertionSort_unsorted(t *testing.T) {
	arr := []int{4, 3, 2, 1}
	expected := []int{1, 2, 3, 4}

	Insertion(arr)

	if !utils.Equals(arr, expected) {
		t.Errorf("Expected %v but found %v", expected, arr)
	}
}

func BenchmarkInsertionSort(b *testing.B) {
	Insertion([]int{4, 3, 2, 1})
}
