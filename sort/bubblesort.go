package sort

import "gitlab.com/FedericoAntoniazzi/go-algorithms/utils"

func Bubble(input []int) {
	swapped := true

	for swapped {
		swapped = false

		for i:=0; i<len(input)-1; i++ {
			if input[i] > input[i+1] {
				utils.SwapPointers(&input[i], &input[i+1])
				swapped = true
			}
		}
	}
}
