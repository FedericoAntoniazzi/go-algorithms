package sort

func mergesort(arr []int, start, end int) {
	if start >= end {
		return 
	}

	var middle = (start+end)/2

	mergesort(arr, start, middle)
	mergesort(arr, middle+1, end)

	merge(arr, start, middle, end)
}

func merge(arr []int, start, middle, end int) {
	dimLeft := middle - start +1
	dimRight := end - middle

	// Temporary slices used as helpers
	left := make([]int, dimLeft)
	right := make([]int, dimRight)

	// Copy left and right slices into temp ones
	for i:=0; i<dimLeft; i++ {
		left[i] = arr[start + i]
	}
	for i:=0; i<dimRight; i++ {
		right[i] = arr[middle + 1 + i]
	}

	// Index to iterate temp slices
	i, j := 0, 0
	// Index for the original array
	k := start

	// Save in arr the items sorted
	for i < dimLeft && j < dimRight {
		if left[i] <= right[j] {
			arr[k] = left[i]
			i++
		} else {
			arr[k] = right[j]
			j++
		}
		k++
	}

	// Copy remaining elements into arr
	for ; i < dimLeft; i++ {
		arr[k] = left[i]
		k++
	}
	for ; j < dimRight; j++{
		arr[k] = right[j]
		k++
	}
}

func Merge(input []int) {
	mergesort(input, 0, len(input)-1)
}
