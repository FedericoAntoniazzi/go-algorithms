package main

import (
	"fmt"

	"gitlab.com/FedericoAntoniazzi/go-algorithms/sort"
)

func main() {
	slice := []int{4,3,2,1,0}

	fmt.Printf("Before sorting: %v\n", slice)

	sort.Merge(slice)

	fmt.Printf("After bubblesort: %v\n", slice);
}
